

public class Calculadora {
    public static void main(String[] args) {
        int num1 = 10;
        int num2 = 20;
        int suma = sumar(num1, num2);
        int resta = restar(num1, num2);
	int multiplica = multiplicar(num1, num2);
	int divide = dividir(num1, num2);
        System.out.println("La suma de " + num1 + " y " + num2 + " es: " + suma);
	System.out.println("La resta de " + num1 + " y " + num2 + " es: " + resta);
	System.out.println("La multiplicación de " + num1 + " y " + num2 + " es: " + multiplica);
	System.out.println("La división de " + num1 + " y " + num2 + " es: " + divide);
    } 

    public static int sumar(int a, int b) {
        return a + b;
    }

    public static int restar(int a, int b) {
        return a - b;
    }

    public static int multiplicar(int a, int b) {
        return a * b;
    }

    public static int dividir(int a, int b) throws ArithmeticException {
        if (b == 0) {
            throw new ArithmeticException("El divisor no pot ser zero");
        }
        return a / b;
    }
}
